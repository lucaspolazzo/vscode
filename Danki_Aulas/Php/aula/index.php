<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Formulário</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <?php
        date_default_timezone_set('America/Sao_Paulo');

        $pdo = new PDO('mysql:host=localhost;dbname=modulo_08', 'root', '', [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
        
        $sql = $pdo->prepare("SELECT * FROM clientes RIGHT JOIN cargos ON clientes.cargo = cargos.id");
        $sql->execute();

        $clientes = $sql->fetchAll();

        foreach ($clientes as $key => $value) {
            echo $value['nome'];
            echo '|';
            echo $value['nome_cargo'];
            echo '<hr>';
        }

        /*
        $sql = $pdo->prepare("SELECT * FROM categorias");

        $sql->execute();

        $info = $sql->fetchAll();

        foreach ($info as $key => $value) {
            $categoria_id = $value['id'];
            echo 'Exibindo posts de: '.$value['nome'];
            echo '<br>';
            $sql = $pdo->prepare("SELECT * FROM posts WHERE categoria_id = $categoria_id");
            $sql->execute();
            $infoPosts = $sql->fetchAll();
            foreach ($infoPosts as $key => $value) {
                echo 'Titulo: '.$value['titulo'];
                echo '<br>';
                echo 'Noticia: '.$value['conteudo'];
                echo '<hr>';
            }

        }

        $sql = $pdo->prepare("SELECT posts.*, categorias.*, posts.id AS post_id FROM posts INNER JOIN categorias ON posts.categoria_id = categorias.id");

        $sql->execute();

        $info = $sql->fetchAll(PDO::FETCH_ASSOC);

        echo '<pre>';
        print_r($info);
        echo '</pre>';
        
        foreach ($info as $key => $value) {
            echo 'Titulo: '.$value['titulo'];
            echo '<br>';
            echo 'Noticia: '.$value['conteudo'];
            echo '<hr>';
        }
        */
    ?>
</body>
</html>